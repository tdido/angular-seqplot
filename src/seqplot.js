'use strict';

angular.module('seqPlot', [])
  .directive('seqplot', function($compile) {
    return {
      restrict: 'AE',
      scope: {
        regions: "=", //the regions array:[{"start":10,"end":20,"color":"#F0F0F0","ann":"NICE","data":{"desc":"A very nice domain","title":"a title"}},...]
        seq: "@", //the sequence we're annotating
        regionData: "=" //a channel to send back data about the region to the main scope
      },
      link: function(scope, element, attrs) {
        scope.$watch(function() {
          var watched = {}; //watch for changes in the regions or seq _mandatory_ attributes
          watched.regions = scope.regions;
          watched.seq = attrs.seq;
          return watched;
        }, function(newVal, oldVal, scope) {

          function deepCopy(o) {
            var copy = o,
              k;

            if (o && typeof o === 'object') {
              copy = Object.prototype.toString.call(o) === '[object Array]' ? [] : {};
              for (k in o) {
                copy[k] = deepCopy(o[k]);
              }
            }
            return copy;
          }

          var regions = newVal.regions;
          var seq = newVal.seq;
          if (seq.length > 0 && regions) {
            var seqlen = seq.length;

            //optional attributes
            var defaultColor = attrs.defaultColor || "gray"; //Default color to assign to regions that don't specify one
            var barwidth = attrs.barWidth || 600; //width of the sequence plot
            var barheight = attrs.barHeight || 10; //height of the sequence plot
            var gradient = attrs.gradient || false; //use a gradient instead of plain colors (resource intensive, don't use if plotting many sequences)

            //width factor to transform the regions length into their pixel equivalent
            var wfactor = barwidth / seqlen;

            //assign a random ID to the element so Raphael can find it
            var randomId = Math.random().toString(36).substring(7);
            element.attr('id', randomId);
            $compile(element.contents())(scope);

            var paper = Raphael(randomId, barwidth, barheight);
            paper.renderfix()
            paper.safari()

            for (var i = 0; i < regions.length; i++) {
              var region = regions[i];
              var start = region.start;
              var end = region.end;
              var ann = region.ann;
              var x1 = (start - 1) * wfactor;
              var x2 = end * wfactor;
              var y1 = 0;
              var midpoint = barheight / 2;
              var fillColor = region.color || defaultColor;

              if (gradient) {
                fillColor = "90-#fff-" + fillColor + ":50-#fff";
              }

              if (end != seqlen) {
                x2 += 1 * wfactor;
              }

              var rect = paper.rect(x1, y1, x2 - x1, barheight, 0);

              rect.data("region", deepCopy(region));

              rect.attr({
                "fill": fillColor
              });
              rect.click(function() {
                var href = this.data("region").href;
                if (href) {
                  window.open(href);
                }
              });
              rect.mouseover(function() {
                this.attr("stroke", "#fff")
                var r = this.data("region");
                var callbackData = {};
                if (r.hasOwnProperty("data")) {
                  callbackData = deepCopy(r.data);
                }
                callbackData.start = r.start;
                callbackData.end = r.end;
                callbackData.seq = seq.slice(r.start - 1, r.end);
                scope.regionData.data = callbackData;
                scope.$apply();
                if (r.hasOwnProperty("href")) {
                  this.attr("cursor", "pointer")
                }
              });
              rect.mouseout(function() {
                scope.regionData.data = null;
                scope.$apply();
                this.attr("stroke", "#000")
                this.attr("cursor", "default")
              });

              var line = paper.path(["M", 0, midpoint, "L", barwidth, midpoint]).toBack();
              line.attr({
                "stroke-width": 0.1
              });
              line.translate(0.5, 0.5);
            }
          }
        }, true);
      }
    };
  });
